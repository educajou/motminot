# Présentation

Motminot est un jeu inspiré de l'ancien jeu télévisé [Motus](https://fr.wikipedia.org/wiki/Motus_(jeu_t%C3%A9l%C3%A9vis%C3%A9)), adapté pour les élèves de nibeau élémentaire.

En six essais il faut deviner un mot. On ne peut proposer que des mots du dictionnaire, accords et conjugaisons comprises. Après chaque proposition, Motminot indiquera les lettres correctes et le cas échéant si elles sont bien placées, façon "Mastermind".

Un mot par jour, le même pour tous. 

![Capture d'écran exemple](https://forge.apps.education.fr/educajou/motminot/-/raw/main/screenshot.png "Capture d'écran")

# Fonctionnement

Motminot fonctionne en HTML et Javascript dans un simple navigateur. Il ne nécessite pas d'environnnement PHP ni de base de données.
On peut l'utiliser en ligne en l'hébergeant sur un serveur web, mais il fonctionne hors connexion, en le lançant depuis un explorateur de fichiers. On peut alors envisager de l'utiliser localement sur une machine isolée, ou bien dans un dossier partagé sur un réseau local.

# Développement

Motminot est une version dérivée de [Motchus](https://motchus.fr/) créé par [@MedericGC](https://twitter.com/MedericGC) et [@ze_armavi](https://twitter.com/ze_armavi), lui même adapté de [Sutom](https://framagit.org/JonathanMM/sutom) depuis [Wordle](https://www.powerlanguage.co.uk/wordle/).

C'est donc le [fork](https://fr.wikipedia.org/wiki/Fork_(d%C3%A9veloppement_logiciel)) du fork d'un fork 😉, rendu possible par les licences libres.

Par rapport à Motchus dont il dérive directement, les modifications (actuelles) sont :

- changement de mot à 8h30 au lieu de minuit
- suppression de la fonction d'affichage des définitions
- suppression du partage de résultats sur Twitter
- thème graphique
- fourniture d'une feuille de calcul pour la préparation des mots
- grille élèves à imprimer

# Installation

Télécharger [l'archive ZIP](https://forge.apps.education.fr/educajou/motminot/-/archive/main/motminot-main.zip) et l'extraire (par exemple, clic droit, extraire tout).

## Utilisation locale

Dans le dossier **Motminot** ouvrir le fichier index.html avec le navigateur web (par exemple Firefox).

## Utilisation sur serveur

Via une connexion (S)FTP déposer le dossier **Motminot**, ou directement son contenu, sur le serveur distant.


# Paramétrage

## Éditer la liste des mots

Avec un éditeur de texte (pas un traitement de texes) comme Notepad++ , éditer le fichier motminot/js/mots/motsATrouver.js à partir de la ligne 16.

**Attention à ne pas confondre motsATrouver.js et motsATrouver.js.map**

**Sous Windows, le gestionnaire de fichiers a tendance à masquer les extensions de fichiers.** Il est conseillé d'en rétablir l'affichage (menu affichage, afficher les extensions de fichier) sans quoi on peut confondre motsATrouver.js (qui apparaît sous le nom "motsATrouver") et "motsATrouver.js.map" qui apparaît sous le nom "motsATrouver.js".

Une liste de mots génériques est fournie avec Motminot.

Les mots doivent être en MAJUSCULES sans accents, entre 4 et 10 lettres, être balisés par des guillemets et séparés par des virgules.

Exemple :

"SOURIS",
"POISSON",
"PANTALON",
"COUVERTURE",
"GOMME",
"ECOLE",

Pour que les mots soient valides ils doivent être contenus dans le dictionnaire de Motminot, qui se trouve dans le fichier listeMotsProposables.js
Si l'on doit ajouter un mot à deviner qui n'y figure pas, il faudra donc penser à l'ajouter simutanément dans listeMotsProposables.js (même méthode).

## Éditer la liste des thèmes

De la même façon il faut éditer le fichier motminot/js/mots/themes.js
(un thème par semaine)

### Tableur

Un fichier **mots.ods** se trouve à la racine du dossier. C'est un outil pour faciliter la préparation des mots.

Il suffit d'entrer les mots dans les cases vertes. Si elles restent vertes c'est que le mot est dans le dictionnaire (vérification automatique par comparaison avec la feuille "dictionnaire").

La première colonne sert à prévoir des thèmes hebdomadaires.

La colonne "mot pour site" est complétée automatiquement d'après les mots des cases vertes, auquels sont ajoutées les marques de balisage nécessaire. Il suffira ainsi de copier-coller les mots depuis cette colonne pour les intégrer facilment dans **motsATrouver.js**.

***Ne pas oublier d'adapter la colonne "dates".***

## Date

Il faut impérativement paramétrer la date du premier jour du jeu, à partir de laquelle seront calculés tous les tirages d'après la liste dans motsATrouver.js.

Pour cela, éditer le fichier /js/dictionnaire.js

Modifier les **lignes 25, 31 et 37** (format année, mois, jour) <span class="rouge"><strong>Attention</strong> le premier mois porte le numéro 0, ainsi février=2, mars=3 ...</span>

Dans cet exemple, la date de référence pour le premier mot est le 4 septembre 2023 :

**var origine = new Date(2023, 8, 4).getTime();**

# Instances

La Cirsonscription de Digne-les-Bains maintient [deux instances](http://www.circo-digne.ac-aix-marseille.fr/applis/) (cycle 2 et cycle 3) de Motminot, avec une invitations aux classes à partager leurs résultats sur [Digipad](https://digipad.app/p/170419/42707071867f3).


<style>
    .rouge {color: red;}
</style>