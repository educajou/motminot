(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var themes = /** @class */ (function () {
        function themes() {
        }
        themes.Liste = [
            "",
            "L'école",
            "La piscine",
            "Le cirque",
            "L'automne",
            "Les couleurs",
            "Les femelles chez les animaux",
            "La cuisine",
            "La météo",
            "Les monstres",
            "Les qualités",
            "L'armistice",
            "Le spectacle vivant",
            "Le parc",
            "Le vélo",
            "Les sports aux Jeux Olympiques",
            "Les types de minerais",
            "L’automobile",
            "Les jeux vidéo",
            "Les animaux marins",
            "Les animaux de la ferme",
            "Les livres",
            "Les vêtements",
            "Les véhicules et les transports",
            "Les sentiments et émotions",
            "Animaux imaginaires",
            "Métiers",
            "Corps humain",
            "Inventions",
            "Outils",
            "Polygones",
            "Légumes",
            "Salle de bain",
            "Chantier",
            "Oiseau",
            "Meubles",
            "Fleurs",
            "Boissons",
            "Arbres",
            "Argent",
            "Planètes",
            "Restaurant",
            "Coiffeur",
            "Au parc",
            "Tête",
            "Vacances",
            "Symboles de la République",
            "été",
            "Sports d’été",
            "Sports nautiques"   
        ];
        return themes;
    }());
    exports.default = themes;
});