var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "./mots/listeMotsProposables", "./mots/motsATrouver", "./mots/definitionMotsATrouver"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var listeMotsProposables_1 = __importDefault(require("./mots/listeMotsProposables"));
    var motsATrouver_1 = __importDefault(require("./mots/motsATrouver"));
    var definitionMotsATrouver_1 = __importDefault(require("./mots/definitionMotsATrouver"));
    var Dictionnaire = /** @class */ (function () {
        function Dictionnaire() {
        }
        Dictionnaire.prototype.getMot = function () {
            var aujourdhui = new Date().getTime();
            var origine = new Date(2022, 7, 28).getTime();
            var numeroGrille = Math.floor((aujourdhui - origine - 30600000) / (24 * 3600 * 1000)) ;
            return motsATrouver_1.default.Liste[numeroGrille % motsATrouver_1.default.Liste.length];
        };
        Dictionnaire.prototype.getDefinition = function () {
            var aujourdhui = new Date().getTime();
            var origine = new Date(2022, 7, 28).getTime();
            var numeroGrille = Math.floor((aujourdhui - origine - 30600000) / (24 * 3600 * 1000)) - 1;
            return definitionMotsATrouver_1.default.Liste[numeroGrille % definitionMotsATrouver_1.default.Liste.length];
        };
        Dictionnaire.prototype.estMotValide = function (mot) {
            mot = this.nettoyerMot(mot);
            return mot.length >= 4 && mot.length <= 10 && listeMotsProposables_1.default.Dictionnaire.includes(mot);
        };
        Dictionnaire.prototype.nettoyerMot = function (mot) {
            return mot
                .normalize("NFD")
                .replace(/[\u0300-\u036f]/g, "")
                .toUpperCase();
        };
        return Dictionnaire;
    }());
    exports.default = Dictionnaire;
});
//# sourceMappingURL=dictionnaire.js.map